from django.conf.urls import url
from .views import update_profile, add_expertise, delete_expertise

urlpatterns = [
    url(r'^update/$', update_profile, name='update_profile'),
    url(r'^add_expertise/$', add_expertise, name='add_expertise'),
    url(r'^delete_expertise/(?P<id>.*)/$', delete_expertise, name='delete_expertise')
]
