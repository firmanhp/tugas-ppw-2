from django.contrib import admin
from .models import Profile, Expertise
# Register your models here.

admin.site.register(Profile)
admin.site.register(Expertise)
