from django.shortcuts import render
from tugas_auth.auth_tools import *
from django.http import HttpResponseRedirect
from django.urls import reverse
from .profile_utils import *
from .models import Profile, Expertise
# Create your views here.

@authenticated
def update_profile(request):
    if request.method != 'POST':
        return HttpResponseRedirect(reverse('main:landing_page'))

    user = get_user(request)
    profile = get_profile_from_user(user)
    if profile is None:
        profile = Profile()

    profile.user = user
    profile.first_name = request.POST['first_name']
    profile.last_name = request.POST['last_name']
    profile.email = request.POST['email']
    profile.batch = request.POST['batch']
    profile.profile_picture_url = request.POST['profile_picture_url']
    profile.linkedin_url = request.POST['linkedin_url']
    profile.save()

    return HttpResponseRedirect(reverse('main:my_profile'))


@authenticated
def add_expertise(request):
    if request.method != 'POST':
        return HttpResponseRedirect(reverse('main:landing_page'))


    expertise = Expertise()
    expertise.user = get_user(request)
    expertise.skill = request.POST['skill']
    expertise.level = request.POST['level']
    expertise.save()

    return HttpResponseRedirect(reverse('main:expertise_settings'))


@authenticated
def delete_expertise(request, id):
    expertise = Expertise.objects.get(id=id)
    if expertise.user.npm == get_npm(request):
        expertise.delete()
        return HttpResponseRedirect(reverse('main:expertise_settings'))

    return HttpResponseRedirect(reverse('main:landing_page'))