from django.db import models
from tugas_auth.models import User

# Create your models here.


class Profile(models.Model):
    user = models.ForeignKey(User)
    first_name = models.CharField("First Name", max_length=128)
    last_name = models.CharField("Last Name", max_length=128)
    email = models.CharField("e-Mail", max_length=128)
    batch = models.IntegerField("Batch", null=False)
    profile_picture_url = models.CharField("Profile Picture URL", max_length=512)
    linkedin_url = models.CharField("LinkedIn URL", max_length=128)


class Expertise(models.Model):
    user = models.ForeignKey(User)
    skill = models.CharField("Skill", max_length=128)
    level = models.CharField("Level", max_length=32)