from django.test import TestCase, Client
from django.urls import reverse
from tugas_auth.models import User
from .profile_utils import *
from .models import Profile, Expertise

# Create your tests here.

def set_logged_in_session(session):
    session['logged_in'] = True
    session['username'] = 'ganteng'
    session['access_token'] = "asdasdasd"
    session['npm'] = "111"
    session['role'] = 'mahasisa'
    session.save()

def set_logged_in_session_2(session):
    session['logged_in'] = True
    session['username'] = 'aaaa'
    session['access_token'] = "zzz"
    session['npm'] = "222"
    session['role'] = 'dosen'
    session.save()

class ProfileUnitTest(TestCase):

    def test_get_profile_from_user(self):
        user = User()
        user.npm = "123123"
        user.username = "susah"
        user.role = "iya"
        user.save()
        self.assertTrue(get_profile_from_user(user) is None)
        profile = Profile()
        profile.user = user
        profile.first_name = "lala"
        profile.last_name = "lili"
        profile.batch = 123123
        profile.save()
        self.assertTrue(get_profile_from_user(user) is not None)

    def test_get_expertises_from_user(self):
        user = User()
        user.npm = "123123"
        user.username = "susah"
        user.role = "iya"
        user.save()
        self.assertTrue(get_expertises_from_user(user) is not None)

    def test_get_one_expertise(self):
        user = User()
        user.npm = "123123"
        user.username = "susah"
        user.role = "iya"
        user.save()
        self.assertTrue(get_one_expertise(user) is None)
        expertise = Expertise()
        expertise.user = user
        expertise.skill = "haha"
        expertise.save()
        self.assertTrue(get_one_expertise(user) is not None)
        expertise = Expertise()
        expertise.user = user
        expertise.skill = "hehe"
        expertise.save()

    def test_update_profile(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        response = client.post(reverse('profile:update_profile'), {
            'first_name':"haha",
            'last_name':"hoho",
            'email':'hahaha,',
            'batch':666,
            'profile_picture_url':'ll',
            'linkedin_url':'10'
        })

        self.assertRedirects(response, reverse('main:my_profile'))



    def test_add_expertise(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        response = client.post(reverse('profile:add_expertise'), {
            'skill': "niup",
            'level': "High"
        })

        self.assertRedirects(response, reverse('main:expertise_settings'))

    def test_delete_expertise(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        response = client.post(reverse('profile:add_expertise'), {
            'skill': "niup",
            'level': "High"
        })
        response = client.get(reverse('profile:delete_expertise', args=(1,)))
        self.assertRedirects(response, reverse('main:expertise_settings'))

    def test_update_profile_get(self):
        client = Client()
        resp = client.get(reverse('profile:update_profile'))
        self.assertRedirects(resp, reverse('main:landing_page'))

    def test_add_expertise_get(self):
        client = Client()
        resp = client.get(reverse('profile:add_expertise'))
        self.assertRedirects(resp, reverse('main:landing_page'))

    def test_delete_expertise_wrong_npm(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        response = client.post(reverse('profile:add_expertise'), {
            'skill': "niup",
            'level': "High"
        })
        client = Client()
        set_logged_in_session_2(client.session)
        response = client.get(reverse('profile:delete_expertise', args=(1,)))
