from .models import Profile, Expertise

def get_profile_from_user(user):
    try:
        return Profile.objects.get(user=user)
    except Profile.DoesNotExist:
        return None


def get_expertises_from_user(user):
    return Expertise.objects.filter(user=user)

def get_one_expertise(user):
    expertises = Expertise.objects.filter(user=user)
    if (len(expertises) > 0):
        return expertises[0]
    else:
        return None
