from django.conf.urls import url
from .views import add_status

urlpatterns = [
    url(r'^add/$', add_status, name='add_status'),
]
