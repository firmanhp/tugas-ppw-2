from django.shortcuts import render
from tugas_auth.auth_tools import *
from .models import Status
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
# Create your views here.



@authenticated
def add_status(request):

    if request.method != 'POST':
        return HttpResponseRedirect(reverse('main:landing_page'))

    status = Status()
    status.user = get_user(request)
    status.content = request.POST['status']
    status.save()

    return HttpResponseRedirect(reverse('main:my_status'))