from django.test import TestCase, Client
from django.urls import reverse
from tugas_auth.models import User

# Create your tests here.

def set_logged_in_session(session):
    session['logged_in'] = True
    session['username'] = 'ganteng'
    session['access_token'] = "asdasdasd"
    session['npm'] = "111"
    session['role'] = 'mahasisa'
    session.save()


class ProfileUnitTest(TestCase):

    def test_add_expertise(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        response = client.post(reverse('status:add_status'), {
            'status': "mantapdjiwa",
        })

        self.assertRedirects(response, reverse('main:my_status'))

    def test_add_expertise_get(self):
        response = Client().get(reverse('status:add_status'))
        self.assertRedirects(response, reverse('main:landing_page'))
