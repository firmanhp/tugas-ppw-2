from django.db import models
from tugas_auth.models import User

# Create your models here.
class Status(models.Model):
    user = models.ForeignKey(User)
    content = models.CharField("Content", max_length=500, null=False)
    created_at = models.DateTimeField(auto_now_add=True)