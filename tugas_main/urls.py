from django.conf.urls import url
from .views import landing_page, dashboard, my_profile, my_history, my_status, my_friends, profile, status, history, other_dashboard, edit_profile, expertise_settings

urlpatterns = [
    url(r'^$', landing_page, name='landing_page'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^dashboard/profile/$', my_profile, name='my_profile'),
    url(r'^dashboard/profile/edit$', edit_profile, name='edit_profile'),
    url(r'^dashboard/profile/expertise$', expertise_settings, name='expertise_settings'),
    url(r'^dashboard/history/$', my_history, name='my_history'),
    url(r'^dashboard/status/$', my_status, name='my_status'),
    url(r'^dashboard/friends/$', my_friends, name='my_friends'),
    url(r'^view/(?P<npm>.*)/$', other_dashboard, name='other_dashboard'),
    url(r'^view/(?P<npm>.*)/profile$', profile, name='profile'),
    url(r'^view/(?P<npm>.*)/status$', status, name='status'),
    url(r'^view/(?P<npm>.*)/history$', history, name='history'),
]
