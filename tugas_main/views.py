from django.shortcuts import render
from tugas_auth.auth_tools import *
from django.http import HttpResponseNotFound
from django.urls import reverse
from tugas_status.models import Status
from tugas_profile.profile_utils import *
from django.contrib.staticfiles.templatetags.staticfiles import static
import requests

# Create your views here.
def landing_page(request):
    if is_logged_in(request):
        return HttpResponseRedirect(reverse('main:dashboard'))
    else:
        return render(request, 'main/landing_page.html', {'profiles': Profile.objects.all()})

@authenticated
def dashboard(request):
    return my_profile(request)

@authenticated
def my_profile(request):
    return profile(request, get_npm(request))

@authenticated
def my_history(request):
    return history(request, get_npm(request))

@authenticated
def my_status(request):
    return status(request, get_npm(request))

@authenticated
def my_friends(request):
    response = dict()
    prepare_response_for_base(request, get_user(request), response)
    response['friend_dict'] = []
    for profile in Profile.objects.all():
        one_expertise = get_one_expertise(profile.user)
        if one_expertise is None:
            one_expertise = "No expertise"
        else:
            one_expertise = one_expertise.skill
        response['friend_dict'].append({
            'friend_name': profile.first_name + " " + profile.last_name,
            'npm': profile.user.npm,
            'angkatan': profile.batch,
            'keahlian': one_expertise
        })
    return render(request, 'main/friends.html', response)

@authenticated
def edit_profile(request):
    response = dict()
    prepare_response_for_base(request, get_user(request), response)
    return render(request, 'main/profile_edit.html', response)

@authenticated
def expertise_settings(request):
    response = dict()
    user = get_user(request)
    response['expertises'] = get_expertises_from_user(user)
    prepare_response_for_base(request, user, response)
    return render(request, 'main/expertise_settings.html', response)

def other_dashboard(request, npm):
    return profile(request, npm)

def profile(request, npm):
    view_user = get_user_from_npm(npm)
    if view_user is None:
        return HttpResponseNotFound()

    response = dict()

    profile = get_profile_from_user(view_user)
    if profile is not None:
        response['first_name'] = profile.first_name
        response['last_name'] = profile.last_name
        response['npm'] = view_user.npm
        response['batch'] = profile.batch
        response['email'] = profile.email
        response['linkedin_url'] = profile.linkedin_url
    else:
        response['first_name'] = "Unknown"
        response['last_name'] = "Unknown"
        response['npm'] = view_user.npm
        response['batch'] = "Unknown"
        response['email'] = "Unknown"
        response['linkedin_url'] = "Unknown"

    response['expertises'] = get_expertises_from_user(view_user)

    prepare_response_for_base(request, view_user, response)
    return render(request, 'main/profile.html', response)


def status(request, npm):
    view_user = get_user_from_npm(npm)
    if view_user is None:
        return HttpResponseNotFound()

    response = dict()
    response['status_list'] = Status.objects.filter(user=view_user).order_by('-created_at')
    prepare_response_for_base(request, view_user, response)
    return render(request, 'main/status.html', response)

def history(request, npm):
    view_user = get_user_from_npm(npm)
    if view_user is None:
        return HttpResponseNotFound()

    response = dict()
    response['histories'] = requests.get('https://private-e52a5-ppw2017.apiary-mock.com/riwayat').json()
    prepare_response_for_base(request, view_user, response)

    return render(request, 'main/history.html', response)


def prepare_response_for_base(request, user_to_view, response):
    own_profile = is_logged_in(request) and (get_npm(request) == user_to_view.npm)
    profile = get_profile_from_user(user_to_view)
    npm = user_to_view.npm
    name = "Unknown"
    profile_picture_url = static('img/PP.jpg')
    if profile is not None:
        name = profile.first_name + " " + profile.last_name
        profile_picture_url = profile.profile_picture_url
    status_count = Status.objects.filter(user=user_to_view).count()
    latest_status = ""
    if status_count > 0:
        latest_status = Status.objects.filter(user=user_to_view).order_by('-created_at')[0].content

    add_dict = {
        'logged_in': is_logged_in(request),
        'own_profile': own_profile,
        'npm': npm,
        'name': name,
        'status_count': status_count,
        'latest_status': latest_status,
        'profile_picture_url': profile_picture_url,
        'needs_to_edit': (own_profile and (profile is None))
    }

    response.update(add_dict)
