from django.test import TestCase, Client

from django.test.utils import override_settings
import tugas_auth.csui_helper as csui_helper
from mock import patch
from django.urls import reverse
from tugas_auth.models import User
from tugas_profile.models import  Profile, Expertise
from tugas_profile.profile_utils import *
from tugas_auth.auth_tools import *

def set_logged_in_session(session):
    session['logged_in'] = True
    session['username'] = 'ganteng'
    session['access_token'] = "asdasdasd"
    session['npm'] = "111"
    session['role'] = 'mahasisa'
    session.save()

class MainTest(TestCase):

    def test_landing_page_not_login(self):
        client = Client()
        resp = client.get(reverse('main:landing_page'))
        self.assertTemplateUsed(resp, 'main/landing_page.html')

    def test_landing_page_logged_in(self):
        client = Client()
        set_logged_in_session(client.session)
        resp = client.get(reverse('main:landing_page'))

    def test_dashboard(self):
        client = Client()
        set_logged_in_session(client.session)
        resp = client.get(reverse('main:dashboard'))
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        resp = client.get(reverse('main:dashboard'))
        profile = Profile()
        profile.user = user
        profile.first_name = "lala"
        profile.last_name = "lili"
        profile.batch = 123123
        profile.save()
        resp = client.get(reverse('main:dashboard'))

    def test_history(self):
        client = Client()
        set_logged_in_session(client.session)
        resp = client.get(reverse('main:my_history'))
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        resp = client.get(reverse('main:my_history'))
        self.assertTemplateUsed(resp, 'main/history.html')

    def test_my_friends(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        profile = Profile()
        profile.user = user
        profile.first_name = "lala"
        profile.last_name = "lili"
        profile.batch = 123123
        profile.save()
        resp = client.get(reverse('main:my_friends'))
        self.assertTemplateUsed(resp, 'main/friends.html')
        expertise = Expertise()
        expertise.user = user
        expertise.skill = "ngakak"
        expertise.level = "Low"
        expertise.save()
        resp = client.get(reverse('main:my_friends'))
        self.assertTemplateUsed(resp, 'main/friends.html')

    def test_edit_profile(self):
        client = Client()
        set_logged_in_session(client.session)
        user = User()
        user.npm = "111"
        user.username = "susah"
        user.role = "iya"
        user.save()
        resp = client.get(reverse('main:edit_profile'))
        self.assertTemplateUsed(resp, 'main/profile_edit.html')

    def test_other_dashboard(self):
        client = Client()
        resp = client.get(reverse('main:other_dashboard', args=(999,)))
        self.assertTrue(resp.status_code == 404)

    def test_status_not_found(self):
        client = Client()
        resp = client.get(reverse('main:status', args=(7,)))
        self.assertTrue(resp.status_code == 404)