from django.apps import AppConfig


class TugasAuthConfig(AppConfig):
    name = 'tugas_auth'
