from django.shortcuts import render
from .auth_tools import *
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
from .csui_helper import get_access_token, verify_user

# Create your views here.


def login(request):
    if (is_logged_in(request)):
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        return_url = request.POST.get('return_url', '/')
        if len(return_url) < 3:
            return_url = reverse('main:landing_page')
        # call csui_helper
        access_token = get_access_token(username, password)
        if access_token is not None:
            ver_user = verify_user(access_token)
            kode_identitas = ver_user['identity_number']
            role = ver_user['role']

            # set session
            request.session['logged_in'] = True
            request.session['username'] = username
            request.session['access_token'] = access_token
            request.session['npm'] = kode_identitas
            request.session['role'] = role

            register_user(request)
            return HttpResponseRedirect(return_url)
        else:
            return HttpResponseRedirect(reverse('auth:login') +
                                        "?return_url=" + return_url +
                                        "&fail=1")
    else:
        return render(request, 'auth/login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect("/")

