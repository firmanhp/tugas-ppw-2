from django.db import models

# Create your models here.
class User(models.Model):
    npm = models.CharField('NPM', max_length=20, primary_key=True)
    username = models.CharField('Username', max_length=200)
    role = models.CharField('Role', max_length=30)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)
