from django.test import TestCase, Client

from django.test.utils import override_settings
import tugas_auth.csui_helper as csui_helper
from mock import patch
from django.urls import reverse
from .auth_tools import *


def mocked_access_token_response(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data):
            self.json_data = json_data

        def json(self):
            return self.json_data

    return MockResponse({
        'access_token': "MOCKTOKEN"
    })


def mocked_fail_access_token_request(*args, **kwargs):
    raise Exception()

def set_logged_in_session(session):
    session['logged_in'] = True
    session['username'] = 'ganteng'
    session['access_token'] = "asdasdasd"
    session['npm'] = "111"
    session['role'] = 'mahasisa'
    session.save()

class AuthTest(TestCase):
    def setUp(self):
        super(AuthTest, self).setUp()
        patcher_custom_auth_get_access_token = patch('tugas_auth.views.get_access_token',
                                                     return_value='MOCK123')
        patcher_custom_auth_get_access_token.start()
        self.addCleanup(patcher_custom_auth_get_access_token.stop)

        patcher_custom_auth_verify_user = patch('tugas_auth.views.verify_user',
                                                return_value={
                                                    'identity_number': '123213',
                                                    'role': 'MOCKTEST',
                                                })
        patcher_custom_auth_verify_user.start()
        self.addCleanup(patcher_custom_auth_verify_user.stop)

        patcher_requests_get = patch('requests.get',
                                     side_effect=mocked_access_token_response)  # just another dummy response
        patcher_requests_get.start()
        self.addCleanup(patcher_requests_get.stop)

        patcher_requests_request = patch('requests.request', side_effect=mocked_access_token_response)
        patcher_requests_request.start()
        self.addCleanup(patcher_requests_request.stop)

    def test_auth_csui_helper_get_access_token_fail(self):
        patcher_request_error = patch('requests.request', side_effect=mocked_fail_access_token_request)
        with patcher_request_error:
            access_token = csui_helper.get_access_token('hahaha', 'hehehe')
            self.assertTrue(access_token is None)

    def test_auth_csui_helper_get_access_token_ok(self):
        access_token = csui_helper.get_access_token('hahaha', 'hehehe')
        self.assertTrue(access_token is not None)

    def test_auth_csui_helper_verify_user(self):
        user_data = csui_helper.verify_user('hahaha')
        self.assertTrue(user_data is not None)

    def test_auth_csui_helper_get_data_user(self):
        user_data = csui_helper.get_data_user('hahaha', '123')
        self.assertTrue(user_data is not None)

    def test_auth_login_already(self):
        client = Client()
        set_logged_in_session(client.session)
        resp = client.get(reverse('auth:login'))
        self.assertTrue(resp, '/')

    def test_auth_cusotm_auth_login_get(self):
        response = Client().get(reverse('auth:login'))
        self.assertTemplateUsed(response, 'auth/login.html')

    def test_auth_custom_auth_login_ok(self):
        client = Client()
        parameters = {
            'username': 'saya',
            'password': 'ganteng'
        }
        response = client.post(reverse('auth:login'), parameters)
        self.assertTrue('logged_in' in client.session)

    def test_auth_custom_auth_login_fail(self):
        patcher_custom_auth_get_access_token = patch('tugas_auth.views.get_access_token', return_value=None)
        with patcher_custom_auth_get_access_token:
            client = Client()
            parameters = {
                'username': 'saya',
                'password': 'jelek',
            }
            response = client.post(reverse('auth:login'), parameters)
            self.assertRedirects(response, reverse('auth:login') + "?return_url=" + reverse('main:landing_page') + "&fail=1")

    def test_auth_custom_auth_logout_ok(self):
        client = Client()
        set_logged_in_session(client.session)
        response = client.get(reverse('auth:logout'))
        self.assertTrue('logged_in' not in client.session)

    @override_settings(TESTING=False)
    def test_auth_custom_auth_authentication_check(self):
        response = Client().get(reverse('main:dashboard'))
        self.assertRedirects(response, reverse('auth:login') + "?return_url=" + reverse('main:dashboard'))

    def test_auth_tools_nonexistant_users(self):
        client = Client()
        set_logged_in_session(client.session)
        response = client.get(reverse('auth:login'))
        self.assertTrue(get_user(response.wsgi_request) is None)
        self.assertTrue(get_user_from_npm("123") is None)