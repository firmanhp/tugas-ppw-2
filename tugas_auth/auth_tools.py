from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import User

def is_logged_in(request):
    return get_session_data(request, 'logged_in', False )


def get_username(request):
    return get_session_data(request, 'username', None)


def get_npm(request):
    return get_session_data(request, 'npm', None)


def get_role(request):
    return get_session_data(request, 'role', None)


def get_session_data(request, param, default):
    return request.session.get(param, default)


def get_user(request):
    try:
        return User.objects.get(npm=get_npm(request))
    except User.DoesNotExist:
        return None


def get_user_from_npm(npm):
    try:
        return User.objects.get(npm=npm)
    except User.DoesNotExist:
        return None


def register_user(request):
    if User.objects.filter(npm=get_npm(request)).count() == 0:
        user = User()
        user.npm=get_npm(request)
        user.username=get_username(request)
        user.role=get_role(request)
        user.save()


def authenticated(func):
    def wrapper(*args, **kwargs):
        if not settings.TESTING and not is_logged_in(args[0]):
            return HttpResponseRedirect(reverse('auth:login') + "?return_url=" + args[0].path)
        return func(*args, **kwargs)
    return wrapper